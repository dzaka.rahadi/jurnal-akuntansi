<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRekeningTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rekening', function (Blueprint $table) {
            $table->integerIncrements('id')->unsigned();
            $table->integer('jurnal_id')->unsigned();
            $table->foreign('jurnal_id')->references('id')->on('jurnal')->onDelete('cascade')->onUpdate('cascade');
            $table->string('nama', 100);
            $table->decimal('saldo', 11, 2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rekening');
    }
}
