
@extends('layout.main')

@section('title', 'Buku Besar')

@section('search')
    <form class="form-inline float-right" method="get" action="/bukubesar">
      <input name="cari" class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      <a href="/bukubesar" class="btn btn-outline-info ml-2">show All</a>
      <a class="btn btn-outline-secondary ml-2" href="/index">Keluar</a>
    </form>
@endsection

@section('container')

<div class="container">
    <div class="row">
        <div class="col-10">
            <h1 class="mt-3">Buku Besar</h1>

            <table rules="all" class="table table-striped table-dark">
                <thead>
                    <tr align="center">
                        <th>ID</th>
                        <th>Wkt_jurnal</th>
                        <th>Keterangan</th>
                        <th>Saldo</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($jurnal as $jrn)
                        <tr align="center">
                            <th scope="row">{{ $loop->iteration }}.</th>
                            <td>{{ $jrn->wkt_jurnal }}</td>
                            <td>{{ $jrn->keterangan }}</td>
                            <td>
                                <table>
                                    <tbody>
                                        @foreach ($jrn->rekening as $rekenings)
                                            <tr>
                                                <td>{{ $loop->iteration }}.</td>
                                                <td>{{ $rekenings->saldo }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
