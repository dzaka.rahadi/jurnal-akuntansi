@extends('layout.main')

@section('title', 'Form Tambah Data')

@section('container')

@if ($errors->any())
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><em>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </em>
    </div>
@endif

<div class="container">
    <div class="row">
        <div class="col-8">
            <h1 class="mt-3">Form Tambah Data</h1>
            <form method="post" action="/jurnal">
                @csrf
                <div class="form-group">
                    <label for="wkt_jurnal">Wkt_jurnal</label>
                    <input type="date" class="form-control @error('wkt_jurnal') is-invalid @enderror" id="wkt_jurnal" placeholder="Masukkan Wkru_jurnal" name="wkt_jurnal">
                </div>
                <div class="form-group">
                    <label for="keterangan">Keterangan</label>
                    <input type="text" class="form-control @error('keterangan') is-invalid @enderror" id="keterangan" placeholder="Masukkan Keterangan" name="keterangan">
                </div>
                <button type="submit" class="btn btn-outline-dark">Tambah Data!</button>

                <a href="/jurnal" class="btn btn-outline-warning">Kembali</a>
            </form>
        </div>
    </div>
</div>

@endsection
