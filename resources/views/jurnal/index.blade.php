
@extends('layout.main')

@section('title', 'Jurnal')

@section('search')
    <form class="form-inline my-2 my-lg-0" method="get" action="/jurnal">
        <input name="cari" class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        <a href="/jurnal" class="btn btn-outline-info ml-2">show All</a>
        <a class="btn btn-outline-secondary ml-2" href="/index">Keluar</a>
    </form>
@endsection

@section('container')

<div class="container">
    <div class="row">
        <div class="col-10">
            <h1 class="mt-3">Jurnal</h1>

            <a href="/jurnal/create" class="btn btn-outline-dark my-3">Tambah Data</a>

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <table rules="all" class="table table-striped table-dark">
                <thead>
                    <tr align="center">
                        <th>ID</th>
                        <th width="200px">Wkt_jurnal</th>
                        <th>Keterangan</th>
                        <th>Item</th>
                        <th>Total</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($jurnal as $no => $jurnals)
                        <tr align="center">
                            <th>{{ $no + $jurnal->firstitem() }}</th>
                            <td align="center">
                                <p>
                                    Ditambahkan :<br><br>
                                    {{ \Carbon\Carbon::parse($jurnals->wkt_jurnal)->format('l, d F Y') }} <br><br><br>
                                    {{ \Carbon\Carbon::parse($jurnals->created_at)->diffForHumans() }}
                                </p>
                            </td>
                            <td align="center">{{ $jurnals->keterangan }}</td>
                            <td>

                                @if(count($jurnals->rekening))
                                <table>
                                    <thead>
                                        <tr align="center">
                                            <th>ID</th>
                                            <th>Nama</th>
                                            <th>Saldo</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($jurnals->rekening as $rekenings)
                                            <tr align="center">
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $rekenings->nama }}</td>
                                                <td>{{ $rekenings->saldo }}</td>
                                                <td>
                                                    <a href="/rekening/{{ $rekenings->id }}/edit" class="btn btn-outline-primary">Edit</a>
                                                    <form action="/rekening/{{ $rekenings->id }}"  method="post" class="d-inline">
                                                        @method('delete')
                                                        @csrf
                                                        <button type="submit" class="btn btn-outline-danger" onclick="return confirm('Apakah Anda Ingin Menghapusya??')">Delete</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @else
                                    <p class="text-center">Item Kosong</p>
                                    <p class="text-center">Item di-isi dulu boyy!!!</p>
                                @endif
                                                <th>{{ $jurnals->total }}<th>

                                <form action="/jurnal/{{ $jurnals->id }}/edit" class="d-inline">
                                    <button type="submit" class="btn btn-outline-primary">Edit</button>
                                </form>

                                <form action="/jurnal/{{ $jurnals->id }}"  method="post" class="d-inline">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-outline-danger" onclick="return confirm('Apakah Anda Ingin Menghapusnya??')">Delete</button>
                                </form>

                                <form action="/rekening/create" class="d-inline">
                                    <button type="submit" class="btn btn-outline-primary">Tambah Item</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="d-block col-12 mt-4">{{ $jurnal->links() }}</div>
        </div>
    </div>
</div>

@endsection
