@extends('layout.main')

@section('title', 'Form Tambah Data')

@section('container')

<div class="container">
    <div class="row">
        <div class="col-8">
            <h1 class="mt-3">Form Tambah Item</h1>
                <form method="post" action="/jurnalitem">
                    @csrf
                    <div class="form-group">
                        <label for="jurnal_id">Jurnal_id</label>
                        <input type="text" class="form-control" id="jurnal_id" placeholder="Masukkan Jurnal_id" name="jurnal_id">
                    </div>
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" class="form-control" id="nama" placeholder="Masukkan Nama" name="nama">
                    </div>
                    <div class="form-group">
                        <label for="saldo">Saldo</label>
                        <input type="text" class="form-control" id="saldo" placeholder="Masukkan Saldo" name="saldo">
                    </div>
                    <button type="submit" class="btn btn-outline-dark">Tambah Data!</button>
                </form>
        </div>
    </div>
</div>

@endsection
