@extends('layout.main')

@section('title', 'Form Tambah Data')

@section('container')

<div class="container">
    <div class="row">
        <div class="col-8">
            <h1 class="mt-3">Form Ubah Item</h1>

            <form method="post" action="/rekening/{{ $rekening->id }}">
                @method('put')
                @csrf
                <div class="form-group">
                    <label for="keterangan">Keterangan</label>
                    <select name="jurnal_id" class="form-control">
                        <option value="">Pilih Keterangan</option>
                        @foreach ($jurnal as $data)
                            <option value="{{ $data->id }}">{{ $data->id }}. {{ $data->keterangan }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" placeholder="Masukkan Nama" name="nama" value="{{ $rekening->nama }}">
                </div>
                <div class="form-group">
                    <label for="saldo">Saldo</label>
                    <input type="decimal" class="form-control" id="saldo" placeholder="Masukkan Saldo" name="saldo" value="{{ $rekening->saldo }}">
                </div>
                <button type="submit" class="btn btn-outline-dark">Tambah Data!</button>
            </form>
        </div>
    </div>
</div>

@endsection
