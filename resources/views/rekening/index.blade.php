
@extends('layout.main')

@section('title', 'Rekening')

@section('search')
    <form class="form-inline my-2 my-lg-0" method="get" action="/rekening">
        <input name="cari" class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        <a href="/rekening" class="btn btn-outline-info ml-2">show All</a>
        <a class="btn btn-outline-secondary ml-2" href="/index">Keluar</a>
    </form>
@endsection

@section('container')

<div class="container">
    <div class="row">
        <div class="col-10">
            <h1 class="mt-3">Rekening</h1>

            <a href="/rekening/create" class="btn btn-outline-dark my-3">Tambah Data</a>

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif


            <table rules="all" class="table table-striped table-dark md-3">
                <thead>
                    <tr align="center">
                        <th>ID</th>
                        <th>Keterangan</th>
                        <th>Nama</th>
                        <th>Saldo</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rekening as $rkn)
                        <tr align="center">
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ $rkn->keterangan }}</td>
                            <td>{{ $rkn->nama }}</td>
                            <td>{{ $rkn->saldo }}</td>
                            <td>
                                <form action="/rekening/{{ $rkn->id }}/edit" class="d-inline">
                                    <button type="submit" class="btn btn-outline-primary">Edit</button>
                                </form>

                                <form action="/rekening/{{ $rkn->id }}"  method="post" class="d-inline">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-outline-danger" onclick="return confirm('Apakah Anda Ingin Menghapusnya??')">Delete</button>
                                </form>

                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            <div class="d-block col-12 mt-4">{{ $rekening->links() }}</div>
        </div>
    </div>
</div>

@endsection


