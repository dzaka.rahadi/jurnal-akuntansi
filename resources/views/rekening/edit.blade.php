@extends('layout.main')

@section('title', 'Form Tambah Data')

@section('container')

@if ($errors->any())
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><em>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </em>
    </div>
@endif

<div class="container">
    <div class="row">
        <div class="col-8">
            <h1 class="mt-3">Form Edit Data</h1>

            <form method="post" action="/rekening/{{ $rekening->id }}">
                @method('put')
                @csrf
                <div class="form-group">
                    <label for="keterangan">Keterangan</label>
                    <select name="jurnal_id" class="form-control">
                        <option value="">Pilih Keterangan</option>
                        @foreach ($jurnal as $data)
                            <option value="{{ $data->id }}">{{ $data->id }}. {{ $data->keterangan }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" placeholder="Masukkan Nama" name="nama" value="{{ $rekening->nama }}">
                </div>
                <div class="form-group">
                    <label for="saldo">Saldo</label>
                    <input type="text" class="form-control @error('saldo') is-invalid @enderror" id="saldo" placeholder="Masukkan Saldo" name="saldo" value="{{ $rekening->saldo }}">
                </div>
                <button type="submit" class="btn btn-outline-dark">Edit Data!</button>

                <a href="/rekening" class="btn btn-outline-warning">Kembali</a>
            </form>
        </div>
    </div>
</div>

@endsection
