<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Jurnal extends Model
{
    protected $table = 'jurnal';
    protected $fillable = ['wkt_jurnal', 'keterangan', 'total'];

    protected $dates = ['wkt_jurnal'];

    public function rekening(){
        return $this->hasMany('App\Rekening');
    }

    public function getWktJurnalAttribute($value){
        return Carbon::parse($value)->format('d-m-Y');
    }
}
