<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RekeningRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'jurnal_id' => 'required',
            'nama' => 'required',
            'saldo' => 'required|numeric'
        ];
    }

    public function messages()
    {
        return [
            'jurnal_id.required' => 'Keterangan Menghilang??',
            'nama.required' => 'Nama mu Siapa??',
            'saldo.required' => 'Saldo mu Berapa??',
            'saldo.numeric' => 'Harus Nomor!!'
        ];
    }
}
