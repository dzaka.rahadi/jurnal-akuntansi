<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JurnalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'wkt_jurnal' => 'required|date',
            'keterangan' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'wkt_jurnal.required|date' => 'Kasih tau Waktu Kauu??',
            'keterangan.required' => 'Mana Keterangan mu??'
        ];
    }
}
