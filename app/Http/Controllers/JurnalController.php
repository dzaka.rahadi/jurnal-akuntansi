<?php

namespace App\Http\Controllers;

use App\Http\Requests\JurnalRequest;
use Carbon\Carbon;
use App\Jurnal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\This;

class JurnalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $jurnal = DB::table('jurnal')
        if($request->has('cari')){
            $jurnal = \App\Jurnal::where('keterangan', 'LIKE', "%" .$request->cari. "%")->paginate();
        }else{
            $jurnal = Jurnal::with('rekening')->paginate(2);
        }
        return view('jurnal/index', ['jurnal'=>$jurnal]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('jurnal/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JurnalRequest $request)
    {
        // $request->validate([
        //     'wkt_jurnal' => 'required|date_format:d-m-Y',
        //     'keterangan' => 'required'
        // ]);
        $this->validate($request, $request->messages());
        Jurnal::create($request->all());
        return redirect('/jurnal')->with('status', 'Data Berhasil Ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Jurnal  $jurnal
     * @return \Illuminate\Http\Response
     */
    public function show(Jurnal $jurnal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Jurnal  $jurnal
     * @return \Illuminate\Http\Response
     */
    public function edit(Jurnal $jurnal)
    {
        return view('jurnal.edit', compact('jurnal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Jurnal  $jurnal
     * @return \Illuminate\Http\Response
     */
    public function update(JurnalRequest $request, Jurnal $jurnal)
    {
        Jurnal::where('id', $jurnal->id)
        ->update([
            'wkt_jurnal' => $request->wkt_jurnal,
            'keterangan' => $request->keterangan
        ]);

        // DB::table('jurnal')->where('id', $request->$id)->update([
        //     'wkt_jurnal' => $request->wkt_jurnal,
        //     'keterangan' => $request->keterangan
        // ]);
        return redirect('/jurnal')->with('status', 'Data Berhasil Diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Jurnal  $jurnal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Jurnal $jurnal)
    {
        Jurnal::destroy($jurnal->id);
        return redirect('/jurnal')->with('status', 'Data Berhasil Dihapus!');
    }
}
