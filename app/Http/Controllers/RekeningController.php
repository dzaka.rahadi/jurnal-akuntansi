<?php

namespace App\Http\Controllers;

use App\Http\Requests\RekeningRequest;
use Carbon\Carbon;
use App\Jurnal;
use App\Rekening;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RekeningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if($request->has('cari')){
            $rekening = DB::table('rekening')
            ->join('jurnal', 'jurnal.id', '=', 'rekening.jurnal_id')
            ->select('rekening.*', 'jurnal.keterangan')->where('nama', 'LIKE', "%" .$request->cari. "%")
            ->paginate();
        }else{
            $rekening = DB::table('rekening')
            ->join('jurnal', 'jurnal.id', '=', 'rekening.jurnal_id')
            ->select('rekening.*', 'jurnal.keterangan')
            ->paginate(3);
        };
        return view('rekening/index', compact('rekening'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $rekening = DB::table('rekening')->get();
        $jurnal = DB::table('jurnal')->get();
        return view('rekening/create', compact('jurnal'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RekeningRequest $request)
    {
        $this->validate($request, $request->messages());
        Rekening::create($request->all());
        return redirect('/rekening')->with('status', 'Data Berhasil Ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rekening  $rekening
     * @return \Illuminate\Http\Response
     */
    public function show(Rekening $rekening)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rekening  $rekening
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jurnal = DB::table('jurnal')->get();
        $rekening = Rekening::findOrFail($id);
        return view('rekening.edit', compact('rekening', 'jurnal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rekening  $rekening
     * @return \Illuminate\Http\Response
     */
    public function update(RekeningRequest $request, Rekening $rekening)
    {
        Rekening::where('id', $rekening->id)
        ->update([
            'jurnal_id' => $request->jurnal_id,
            'nama' => $request->nama,
            'saldo' => $request->saldo
        ]);

        // DB::table('rekening')->where('id', $request->$id)->update([
        //     'jurnal_id' => $request->jurnal_id,
        //     'nama' => $request->nama,
        //     'saldo' => $request->saldo
        // ]);
        return redirect('/rekening')->with('status', 'Data Berhasil Diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rekening  $rekening
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rekening $rekening)
    {
        Rekening::destroy($rekening->id);
        return redirect()->back()->with('status', 'Data Berhasil Dihapus!');
    }
}
