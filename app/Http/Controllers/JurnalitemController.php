<?php

namespace App\Http\Controllers;

// use App\Jurnalitem;
use App\Rekening;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JurnalitemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Jurnalitem/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('rekening')->insert([
            'jurnal_id' => $request->jurnal_id,
            'nama' => $request->nama,
            'saldo' => $request->saldo
        ]);
            return redirect('/jurnal')->with('Item Berhasil Ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Jurnalitem  $jurnalitem
     * @return \Illuminate\Http\Response
     */
    public function show(Rekening $rekening)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Jurnalitem  $jurnalitem
     * @return \Illuminate\Http\Response
     */
    public function edit(Rekening $rekening)
    {
        return view('jurnalitem.edit', ['rekening'=>$rekening]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Jurnalitem  $jurnalitem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rekening $rekening)
    {
        Rekening::where('id', $rekening->id)
        ->update([
            'jurnal_id' => $request->jurnal_id,
            'nama' => $request->nama,
            'saldo' => $request->saldo
        ]);
        return redirect('/jurnal')->with('Item Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Jurnalitem  $jurnalitem
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rekening $rekening)
    {
        Rekening::destroy($rekening->id);
        return redirect('/jurnal')->with('Data Berhasil Dihapus!');
    }
}
