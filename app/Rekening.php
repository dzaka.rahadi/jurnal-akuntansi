<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rekening extends Model
{
    protected $table = 'rekening';
    protected $fillable = ['jurnal_id', 'nama', 'saldo'];

    public function jurnal(){
        return $this->hasOne('App\Jurnal');
    }
}
