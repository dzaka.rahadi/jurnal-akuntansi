<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jurnalitem extends Model
{
    protected $table = 'rekening';
    protected $fillable = ['jurnal_id', 'nama', 'saldo'];
}
