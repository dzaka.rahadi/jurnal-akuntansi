<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//login 1
Route::get('/register', 'AuthController@getRegister')->middleware('guest')->name('register');
Route::post('/register', 'AuthController@postRegister')->middleware('guest');
Route::get('/login', 'AuthController@getLogin')->middleware('guest')->name('login');
Route::post('/login', 'AuthController@postLogin')->middleware('guest');
Route::get('/index', function(){
    return view('index');
})->middleware('auth')->name('index');
Route::get('/logout', 'AuthController@logout')->middleware('auth')->name('logout');

// Rekening
Route::get('/rekening', 'RekeningController@index')->middleware('auth');
Route::get('/rekening/create', 'RekeningController@create')->middleware('auth');
Route::post('/rekening', 'RekeningController@store');
Route::delete('/rekening/{rekening}', 'RekeningController@destroy');
Route::get('/rekening/{id}/edit', 'RekeningController@edit')->middleware('auth');
Route::put('/rekening/{rekening}', 'RekeningController@update');

// Jurnal
Route::get('/jurnal', 'JurnalController@index')->middleware('auth');
Route::get('/jurnal/create', 'JurnalController@create')->middleware('auth');
Route::post('/jurnal', 'JurnalController@store');
Route::delete('/jurnal/{jurnal}', 'JurnalController@destroy');
Route::get('/jurnal/{jurnal}/edit', 'JurnalController@edit')->middleware('auth');
Route::put('/jurnal/{jurnal}', 'JurnalController@update');

//Jurnal Item
// Route::get('/jurnal/jurnalitem/tambah', 'JurnalitemController@create');
// Route::post('/jurnalitem', 'JurnalitemController@store');
// Route::delete('/jurnal/{rekening}', 'JurnalitemController@destroy');
// Route::get('/jurnal/{rekening}/edititem', 'JurnalitemController@edit');
// Route::put('/jurnal/{rekening}', 'JurnalitemController@update');

//Bukubesar
Route::get('/bukubesar', 'BukubesarController@index')->middleware('auth');

//coba carbon
Route::get('/time', function(){
    $dt = new Carbon();
    $dt->timezone('Asia/Jakarta');

    // echo $dt;

    // echo "<br>";

    // echo $dt->today();

    // echo "<br>";

    // echo $dt->yesterday();

    // echo "<br>";

    // echo $dt->tomorrow();

    // echo "<br>";

    // $newYear = new Carbon('First day Of March 2020');

    // echo $newYear->diffForHumans();

    // echo "<br>";

    //MORE CONTROL OVER DATE
    // echo Carbon::createFromDate(2020, 3, 11, 'Asia/Jakarta');

    // echo "<br>";

    // echo Carbon::create(2020, 3, 11, 12, 45, 5, 'Asia/Jakarta');

    // echo "<br>";

    //GETTERS AND SETTERS
    // echo "<br>";

    // $dt->year = 2025;

    // echo "<br>";

    // echo $dt->year;
    // echo "<br>";

    // echo $dt->second;
    // echo "<br>";

    // echo $dt->daysInMonth;
    // echo "<br>";

    //FORMATING
    // echo  $dt->toDayDateTimeString();

    //     echo "<br>";

    // echo  $dt->toFormattedDateString();

    // echo "<br>";

    // echo $dt->format('h:i:s A');

    //SOME HUMAn READABLE
    $dt = Carbon::now();

    // $dt->subMonth()->diffForHumans();

    echo $dt->addMonth()->diffForHumans();
});
